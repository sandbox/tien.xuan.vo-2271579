(function($) {
  Drupal.behaviors.acceptTermsAndConditions = {
    attach: function(context, settings) {
      $('.terms-and-conditions-modal .accept-button').once('terms-and-conditions-modal', function(context) {
        $(this).click(function() {
          var modal = $(this).parents('.terms-and-conditions-modal');
          modal.modal('hide');
          modal.parent().find('input[type="checkbox"]').prop('checked', true);
        });
      });
    }
  };
})(jQuery);
