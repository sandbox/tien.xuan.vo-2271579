********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: Terms and Conditions
Author: Vo Xuan Tien
Sponsor: Go1 [www.go1.com.au]
Drupal: 7
********************************************************************
DESCRIPTION:

    A simple module which creates Terms & Conditions field (and form element),
    so that administrator can add it to any form, and prevent the form from
    submission until someone accept the T&C.

    Each time a new version of the T&C on each field is created all entities
    will be required to accept the new version.

    Note: T&C text should only be entered by administrators or other highly
    trusted users. filter_xss_admin() is used to filter content for display,
    this is a very permissive XSS/HTML filter intended for admin-only use.

********************************************************************
INSTALLATION:

    Note: It is assumed that you have Drupal up and running.  Be sure to
    check the Drupal web site if you need assistance.  If you run into
    problems, you should always read the INSTALL.txt that comes with the
    Drupal package and read the online documentation.

	1. Place the entire terms_and_conditions directory into your Drupal
        modules/directory.

	2. Enable the terms_and_conditions module by navigating to:

	   Administration > Modules

	Click the 'Save configuration' button at the bottom to commit your
    changes.
