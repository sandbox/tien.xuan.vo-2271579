<?php

/**
 * @file
 * Holds entity module's theme functions.
 */

/**
* Theme the terms and conditions text.
*
* @param $variables
*   An associative array of variables for themeing, containing:
*    - text: Long terms and conditions text.
*
* @ingroup themeable
*/
function theme_terms_and_conditions_textarea($variables) {

  $element = array(
    '#type'          => 'textarea',
    '#title'         => t('Terms & Conditions'),
    '#default_value' => $variables['text'],
    '#value' => $variables['text'],
    '#rows'          => 10,
    '#attributes'    => array('readonly' => 'readonly'),
  );
  return drupal_render($element);
}
