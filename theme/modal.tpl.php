<?php $modal_title_id = drupal_html_id('terms-and-conditions-modal-title'); ?>
<div class="terms-and-conditions-modal modal fade in" id="<?php print $modal_id; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php print $modal_title_id; ?>" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="<?php print $modal_title_id; ?>">Terms and Conditions</h4>
      </div>
      <div class="modal-body">
        <?php print theme('terms_and_conditions_textarea', array('text' => $text)); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary accept-button">Accept</button>
      </div>
    </div>
  </div>
</div>
